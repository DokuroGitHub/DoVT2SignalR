# SignalR

## test postman

connect: <wss://localhost:7001/users>
send message:

```json
// handshake
{
    "protocol": "json",
    "version": 1
}

// join group
// UsersChanged is group name
// 0 is connection id
// JoinGroup is method name
// UsersChanged is method argument
{
    "type": 1,
    "invocationId": "0",
    "target": "JoinGroup",
    "arguments": [
        "UsersChanged"
    ]
}

// create user
{
    "type": 1,
    "invocationId": "0",
    "target": "Create",
    "arguments": [
        {
            "FirstName": "haha",
            "LastName": "hehe",
            "Email": "hehe@gmail.com",
            "Username": "hehehe",
            "Password": "hehehe"
        }
    ]
}

// update user
{
    "type": 1,
    "invocationId": "0",
    "target": "Update",
    "arguments": [
        3,
        {
            "FirstName": "haha",
            "LastName": "hehe"
        }
    ]
}

// delete user
{
    "type": 1,
    "invocationId": "0",
    "target": "Delete",
    "arguments": [
        3
    ]
}

```

## cmd

```bash
cd BE/_4.Api
dotnet watch
cd..
cd..
cd FE/Blazor
dotnet watch

dotnet new gitignore
git init
git add .
git commit -m "first commit"
git push --set-upstream "https://gitlab.com/DokuroGitHub/DoVT2SignalR.git" master

```
