# Clean Architecture

```bash
# Change DefaultConnection at src._4.Api.appsettings.Development.json.ConnectionStrings.DefaultConnection

cd BE

# migration
dotnet ef migrations add InitDb -p "_3.Infrastructure" -s "_4.Api"

dotnet ef database update -p "_3.Infrastructure" -s "_4.Api"

cd "_4.Api"
dotnet watch

```
