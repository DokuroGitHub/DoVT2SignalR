﻿using ApiWrapper.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ApiWrapper;

public static partial class ServicesExtensions
{
    public static IServiceCollection AddApiWrapperServices(
        this IServiceCollection services)
    {
        // add services
        services.AddHttpClient<IChartService, ChartService>();
        services.AddScoped<IChartService, ChartService>();

        return services;
    }
}
