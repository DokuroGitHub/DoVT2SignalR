﻿namespace ApiWrapper.Services;

public class ChartService : IChartService
{
    private string _baseUrl { get; set; }
    private HttpClient _client { get; set; }

    public ChartService(
        IHttpClientFactory httpClientFactory
    )
    {
        _client = httpClientFactory.CreateClient();
        _baseUrl = "https://localhost:7001/api/chart";
    }

    public async Task Get()
    {
        var result = await _client.GetAsync(_baseUrl);
    }
}
